from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse
from .models import GroceryItem
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

def index(request):
    grocery_list = GroceryItem.objects.all()
    context = {'grocery_list': grocery_list,
               "user" : request.user
               }
    return render(request, "djangopractice/index.html", context)

def groceryitem(request, groceryitem_id):
    # response = "You are viewing the grocery details of %s"
    # return HttpResponse(response %groceryitem_id)
    groceryitems = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
    return render(request, "djangopractice/groceryitem.html", groceryitems)

def register(request):
    users = User.objects.all()
    is_user_registered = False
    context = {
        "is_user_registered" : is_user_registered
    }

    # check if the username "johndoe" already exist
    # if it exist change the value of variable is_user_registered to True
    for indiv_user in users:
        if indiv_user.username == "janedoe":
            is_user_registered = True
            break
    
    if is_user_registered == False:
        user = User()
        user.username = "janedoe"
        user.first_name = "Jane"
        user.last_name = "Doe"
        user.email = "jane@mail.com"
        # The set_password is used to ensure that the password is hashed using Django's authentication framework
        user.set_password("jane1234")
        user.is_staff = False
        user.is_active = True
        user.save()

    context = {
        "first_name" : indiv_user.first_name,
        "last_name" : indiv_user.last_name
    }

    return render(request, "djangopractice/register.html", context)

def change_password(request):

    is_user_authenticated = False

    user = authenticate(username="janedoe", password="janedoe1")
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username='janedoe')# this is where we get the user with username 'John Doe'
        authenticated_user.set_password("janedoe1") # This is where we change the password
        authenticated_user.save()
        is_user_authenticated = True
    
    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "djangopractice/change_password.html", context)

def login_view(request):
    username = "janedoe"
    password = "janedoe1"
    user = authenticate(username=username, password=password)

    context={
        "is_user_authenticated": False
    }
    print(user)
    if user is not None:
        login(request, user)
        return redirect("index")
    
    else:
        return render(request, "djangopractice/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("index")

